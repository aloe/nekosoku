//
//  Api.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation

class Api{
    
    class func url(path:String)->String{
        return AppConfig.apiBase() + "/" + path
    }
    
}