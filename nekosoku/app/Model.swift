//
//  Model.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation
//import CloudKit

typealias ModelCompleteCallback = ()->()
typealias ModelArticleListCallback = (articleList:ArticleList)->()
private let isInitializedKey:String = "isInitializedKey"
private let apiJsonStringKey:String = "apiJsonStringKey"
private let favoritIdListKey:String = "favoritIdListKey"

class Model {

    private var articleList:ArticleList = ArticleList()
    private var siteList:SiteList = SiteList()
    
    // http://qiita.com/susieyy/items/acb3bc80a1dafe64cffd
    class var sharedInstance : Model {
    struct Static {
        static let instance : Model = Model()
        }
        return Static.instance
    }
    
    private init(){
        
    }
    
    func toggleFavorit(article:Article){
        article.favorit = !article.favorit
        
        let ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        var tmp:NSMutableArray = self.favoritIdList().mutableCopy() as NSMutableArray
        if article.favorit{
            tmp.addObject(NSNumber(integer: article.id))
        }else{
            tmp.removeObject(NSNumber(integer: article.id))
        }
        
        let ary = NSArray(array: tmp)
        ud.setObject(ary, forKey: favoritIdListKey)
        ud.synchronize()
    }
    
    private func favoritIdList()->NSArray{
        var ary:NSArray? = NSUserDefaults.standardUserDefaults().arrayForKey(favoritIdListKey)
        if ary == nil{
            ary = NSArray()
        }
        
        println("favoritList:\(ary)")
        return ary!
    }
    
    func search(keyword:String, callback:ModelArticleListCallback){
        
        subThread { () -> () in
            let foundList:ArticleList = ArticleList()
            for index in 0..<self.articleList.count(){
                let article:Article = self.articleList.get(index)
                if article.hasKeyword(keyword){
                    foundList.add(article)
                }
            }
            
            mainThread { () -> () in
                callback(articleList: foundList)
            }
            
        }
        
    }
    
    func createFavoritList(callback:ModelArticleListCallback){
        subThread { () -> () in
            let favoritList:ArticleList = ArticleList()
            for index in 0..<self.articleList.count(){
                let article:Article = self.articleList.get(index)
                if(article.favorit){
                    favoritList.add(article)
                }
            }
            
            mainThread{ () -> () in
                callback(articleList:favoritList)
            }
        }
    }
    
//    func ckTest(){
//        
//        let c:CKContainer = CKContainer.defaultContainer()
//        
//        c.accountStatusWithCompletionHandler { (status, error) -> Void in
//            if error != nil{
//                println("Error:\(error)")
//            }
//            println("status\(status)")
//            if status == CKAccountStatus.Available{
//                println("Available")
//            }else if status == CKAccountStatus.CouldNotDetermine{
//                println("CouldNotDetermine")
//            }else if status == CKAccountStatus.NoAccount{
//                println("NoAccount")
//            }else if status == CKAccountStatus.Restricted{
//                println("Restricted")
//            }
//        }
//        
//        CKContainer* c = [CKContainer defaultContainer];
//        [c accountStatusWithCompletionHandler:^(CKAccountStatus status, NSError* err){
//            [c accountStatusWithCompletionHandler:^(CKAccountStatus status, NSError* err){
//            if( status == CKAccountStatusAvailable ) {
//            [self saveSomeDatesWithContainer:c];
//            } else {
//            [[[UIAlertView alloc] initWithTitle:@"ICLOUD" message:@"iCloud is not correctly configured" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
//            }
//            }];
        
//        let database : CKDatabase = CKContainer.defaultContainer().publicCloudDatabase
//        
//        // レコードを新規作成する
//        let record = CKRecord(recordType: "Memo")
//        record.setObject("marimo", forKey: "content")
//        
//        // レコードを保存する
//        database.saveRecord(record) {
//            record, error in
//            if error == nil {
//                // 成功
//                println("Saved : \(record)")
//            } else {
//                // 失敗
//                println("Error : \(error)")
//            }
//        }
//        
//        let predicate = NSPredicate(format: "content != %@", "nil")
//        let query : CKQuery = CKQuery(recordType: "Memo", predicate: predicate)
//        database.performQuery(query, inZoneWithID: nil, {
//            results, error in
//            if error != nil {
//                println("read Error: \(error)")
//            }else{
//                println("readed")
//                println("\(results)")
//            }
//        })
//    }
    
    func setup(callback:ModelCompleteCallback, failCallback:ModelCompleteCallback){
        if(!isInitialized()){
            updateModel(callback, failCallback: failCallback)
            return
        }
        let apiJsonString:String = NSUserDefaults.standardUserDefaults().stringForKey(apiJsonStringKey)!
        let apiDic:NSDictionary = AloeDataUtil.dataToJsonDic(apiJsonString.dataUsingEncoding(NSUTF8StringEncoding)!)
        articleList = ArticleList(nsarray: apiDic.objectForKey("article") as NSArray)
        siteList = SiteList(nsarray: apiDic.objectForKey("site") as NSArray)
        mergeFavorit()
        mergeArticle()
        self.setRootSite()
        if(AloeUtil.isJaDevice()){
            self.setAdSite()
        }
        
        mainThread { () -> () in
            callback()
        }
    }
    
    private func mergeFavorit(){
        let idList:NSArray = self.favoritIdList()
        for index in 0..<idList.count{
            let favoritId = Int(idList[index].intValue)
            for i in 0..<articleList.count(){
                let article = articleList.get(i)
                if(article.id == favoritId){
                    article.favorit = true
                    continue
                }
            }
        }
    }
    
    private func setRootSite(){
        let site:Site = Site.createRootSite()
        
        let photoArticleList:ArticleList = ArticleList()
        for index in 0..<articleList.count(){
            let article:Article = articleList.get(index)
            if(article.hasImage()){
                photoArticleList.add(article)
            }
        }
        
        site.articleList = photoArticleList
        siteList.addAt(site, index: 0)
    }
    
    private func setAdSite(){
        let site:Site = Site.createAdSite()
        siteList.add(site)
    }
    
    func getArticleList()->ArticleList{
        return articleList
    }
    
    func getSiteList()->SiteList{
        return siteList
    }
    
    func isInitialized()->Bool{
        return NSUserDefaults.standardUserDefaults().boolForKey(isInitializedKey)
    }
    
    func updateModel(callback:ModelCompleteCallback, failCallback:ModelCompleteCallback){
        let c:AloeURLLoadCommand = AloeURLLoadCommand(url_: Api.url(""))
        c.setCompleteBlock { (data) -> () in
            let apiJsonString:String? = AloeDataUtil.dataToString(data)
            if(apiJsonString == nil){
                mainThread({ () -> () in
                    failCallback()
                })
                return
            }
            
            println("5")
            let ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            ud.setObject(apiJsonString, forKey: apiJsonStringKey)
            ud.setBool(true, forKey: isInitializedKey)
            ud.synchronize()
            
            self.setup(callback, failCallback)
        }
        
        c.setFailBlock { (error) -> () in
            println("fail update")
            mainThread({ () -> () in
                failCallback()
            })
        }
        
        c.execute()
    }
    
    private func mergeArticle(){
        for(var j:Int=0,jMax:Int=siteList.count();j<jMax;j++){
            var site:Site = siteList.get(j)
            var aList:ArticleList = ArticleList()
            site.setArticleList(aList)
            
            for(var i:Int=0,max:Int=articleList.count();i<max;i++){
            var article:Article = articleList.get(i)
                if(article.siteId == site.id){
                    aList.add(article)
                }
            }
        }
    }
}
