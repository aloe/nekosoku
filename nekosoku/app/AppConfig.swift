//
//  AppConfig.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation

let ON_SHOW_DETAIL_EVENT:String = "onShowDetailEvent"
let ON_HIDE_DETAIL_EVENT:String = "onHideDetailEvent"
let ON_SCROLL_DOWN_EVENT:String = "onScrollDownEvent"
let ON_SCROLL_UP_EVENT:String = "onScrollUpEvent"

let AD_PER_LIST:Int = 10
let ADSTIR_PER_AD:Int = 3

let GAI_TRACKING_ID:String = "UA-55357626-1"

let ADSTIR_MEDIA_ID:String = "MEDIA-4139d2"
let ADSTIR_SPOT_NO:UInt = 1

class AppConfig{
    
    class func apiBase()->String{
        return "http://nekosoku.nowhappy.info/api"
    }
    
}