//
//  NekoUtil.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/29.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

private var rootViewController:UIViewController?

class NekoUtil{
    
    class func setup(rootViewController_:UIViewController){
        rootViewController = rootViewController_
        let gai:GAI = GAI.sharedInstance()
        gai.trackUncaughtExceptions = true
        gai.dispatchInterval = 10
//        gai.logger.logLevel = GAILogLevel.Verbose
        gai.trackerWithTrackingId(GAI_TRACKING_ID)
    }
    
    class func getRootViewController()->UIViewController{
        return rootViewController!
    }
    
    class func cellHeight()->CGFloat{
        return AloeDeviceUtil.windowWidth() / 1.5
    }
    
    class func trackScreen(screenName:String){
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: screenName)
        tracker.send(GAIDictionaryBuilder.createAppView().build())
    }
    
}