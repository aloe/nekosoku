//
//  Page.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

protocol PageDelegate{
    
}

class Page: NSObject, ArticleListDelegate {
    
    let navigation:UINavigationController = UINavigationController()
    let delegate:PageDelegate
    let articleListViewController:ArticleListViewController = ArticleListViewController()
    let overlay:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), AloeDeviceUtil.windowHeight()))
    let statusBg:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowHeight(), 20))
    var site:Site?
    var isModal:Bool = false
    
    init(delegate:PageDelegate){
        self.delegate = delegate
        super.init()
        
        navigation.setNavigationBarHidden(true, animated: false)
        articleListViewController.setup(self)
        navigation.pushViewController(articleListViewController, animated: false)
        
        statusBg.backgroundColor = UIColor.whiteColor()
        statusBg.alpha = 0.5
        navigation.view.addSubview(statusBg)
        
        overlay.backgroundColor = UIColor.blackColor()
        overlay.alpha = 0
        overlay.userInteractionEnabled = false
        navigation.view.addSubview(overlay)
        
    }
    
    func onSelectArticle(article: Article) {
        let webViewController:ArticleWebViewController = ArticleWebViewController()
        webViewController.setup()
        navigation.pushViewController(webViewController, animated: true)
        AloeThreadUtil.wait(0.0, block: { () -> () in
            webViewController.reload(article)
        })
        if !isModal{
            AloeEventUtil.dispatchGlobalEvent(ON_SHOW_DETAIL_EVENT)
        }
        
        NekoUtil.trackScreen(article.title)
    }
    
    func getView()->UIView{
        return navigation.view
    }
    
    func getViewController()->UIViewController{
        return navigation
    }
    
    func dark(alpha:CGFloat){
        overlay.alpha = alpha * 5.0
    }
    
    func reload(site:Site){
        self.site = site
        println("reload:\(site.name)")
        articleListViewController.reload(site)
    }
    
    func toTop(){
        articleListViewController.toTop()
    }
    
    func modal(){
        statusBg.backgroundColor = UIColorFromHex(0x252525)
        
        let closeButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        closeButton.setImage(UIImage(named: "closeButton"), forState: UIControlState.Normal)
        closeButton.frame = CGRectMake(AloeDeviceUtil.windowWidth()-45-5, (AloeDeviceUtil.windowHeight()-45)/2, 45, 45)
        closeButton.addTarget(self, action: "tapClose", forControlEvents: UIControlEvents.TouchUpInside)
        navigation.view.addSubview(closeButton)
        articleListViewController.modal()
        isModal = true
    }
    
    func favorit(){
        articleListViewController.favorit()
    }
    
    func tapClose(){
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.Default, animated: true)
        navigation.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
}
