//
//  Mediator.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit


class Mediator:NSObject, PageDelegate, UIScrollViewDelegate, SearchViewDelegate{
    
    private let window:UIWindow
    private let rootViewController:RootViewController = RootViewController()
    private let sv:UIScrollView = UIScrollView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), AloeDeviceUtil.windowHeight()))
    private let backView:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), AloeDeviceUtil.windowHeight()))
    private let pager:Pager = Pager()
    private var pageList:Array<Page> = Array<Page>()
    private var siteCount:Int = 0
    private let floatingBox:UIView = UIView()
    private let toTopButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    private let searchButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    private let favoritButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    private let searchView:SearchView = SearchView()
    private let nativeAdView:NativeAdView = NativeAdView()
    
    init(window:UIWindow){
        self.window = window
        super.init()
        
//        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        NekoUtil.setup(rootViewController)
        AloeUtil.setup()
        window.rootViewController = rootViewController
        sv.pagingEnabled = true
        pager.setup()
        let rootView:UIView = rootViewController.view
        rootView.addSubview(backView)
        rootView.addSubview(sv)
        
        rootView.backgroundColor = UIColor.blackColor()
        sv.delegate = self
        sv.showsHorizontalScrollIndicator = false
        
        floatingBox.frame = CGRectMake(AloeDeviceUtil.windowWidth()-45-5, (AloeDeviceUtil.windowHeight()-(55*3)) / 2, 45, (55*3))
        rootView.addSubview(floatingBox)
        
        var buttonFrame:CGRect = CGRectMake(0, 0, 45, 45)
        toTopButton.setImage(UIImage(named: "toTopButton"), forState: UIControlState.Normal)
        toTopButton.frame = buttonFrame
        toTopButton.addTarget(self, action: "toTop", forControlEvents: UIControlEvents.TouchUpInside)
        floatingBox.addSubview(toTopButton)
        buttonFrame.origin.y += 55
        
        searchButton.setImage(UIImage(named: "searchButton"), forState: UIControlState.Normal)
        searchButton.frame = buttonFrame
        searchButton.addTarget(self, action: "toSearch", forControlEvents: UIControlEvents.TouchUpInside)
        floatingBox.addSubview(searchButton)
        buttonFrame.origin.y += 55
        
        favoritButton.setImage(UIImage(named: "favoritButton"), forState: UIControlState.Normal)
        favoritButton.frame = buttonFrame
        favoritButton.addTarget(self, action: "toFavorit", forControlEvents: UIControlEvents.TouchUpInside)
        floatingBox.addSubview(favoritButton)
        buttonFrame.origin.y += 55
        
        rootView.addSubview(nativeAdView.getView())
        
        rootView.addSubview(pager.getView())
        
        searchView.setup()
        searchView.delegate = self
        rootView.addSubview(searchView.getView())
        
        // event
        AloeEventUtil.addGlobalEventListener(self, selector: "onShowDetail", name: ON_SHOW_DETAIL_EVENT)
        AloeEventUtil.addGlobalEventListener(self, selector: "onHideDetail", name: ON_HIDE_DETAIL_EVENT)
        AloeEventUtil.addGlobalEventListener(self, selector: "onScrollUp", name: ON_SCROLL_UP_EVENT)
        AloeEventUtil.addGlobalEventListener(self, selector: "onScrollDown", name: ON_SCROLL_DOWN_EVENT)
        
        NekoUtil.trackScreen("initial")
        AloeUtil.activeApp()
        
        if(!Model.sharedInstance.isInitialized()){
            // 初期データ投入
            floatingBox.alpha = 0
            rootViewController.showLoading()
            self.updateModel()
            return
        }
        // 二回目以降
        Model.sharedInstance.setup({ () -> () in
            self.siteCount = Model.sharedInstance.getSiteList().count()
            self.setupPages()
            AloeThreadUtil.wait(0.2, block: { () -> () in
                self.updateModel()
            })
            
            }, failCallback: { () -> () in
                
        })
    }
    
    func updateModel(){
        println("updateModel")
        Model.sharedInstance.updateModel({ () -> () in
            self.siteCount = Model.sharedInstance.getSiteList().count()
            self.setupPages()
            Ad.sharedInstance.reload()
            self.rootViewController.hideLoading()
            self.showToTopButton()
            }, failCallback: { () -> () in
                
        })
    }
    
    func tryShowReview(){
        let noReviewKey:String = "noReviewKey"
        
        if(NSUserDefaults.standardUserDefaults().boolForKey(noReviewKey)){
            println("noreview")
            return
        }
        
        let activeCount = AloeUtil.activeAppCount()
        println("activeCount:\(activeCount)")
        if(!(activeCount == 5 || activeCount == 20 || activeCount == 50)){
            return
        }
        
        AloeUtil.activeApp()
        
        let title:String = "ご利用ありがとうございます"
        let message:String = "使い心地はいかがでしょうか。\nお手すきのときにAppStoreにてレビューを頂ければ幸いです。"
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "レビューする", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let url:String = "https://itunes.apple.com/jp/app/id925576972?l=ja&ls=1&mt=8"
            AloeUtil.openBrowser(url)
            let ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            ud.setBool(true, forKey: noReviewKey)
            ud.synchronize()
        }))
        alert.addAction(UIAlertAction(title: "あとで", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
        }))
        alert.addAction(UIAlertAction(title: "レビューしない", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
            let ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            ud.setBool(true, forKey: noReviewKey)
            ud.synchronize()
        }))
        
        rootViewController.presentViewController(alert, animated: true) { () -> Void in
            
        }
    }
    
    func setupPages(){
        
        let siteList:SiteList = Model.sharedInstance.getSiteList()
        var x:CGFloat = 0
        for index in 0..<siteList.count(){
            if(index < pageList.count){
                x += AloeDeviceUtil.windowWidth()
                continue
            }
            let page:Page = Page(delegate: self)
            let v:UIView = page.getView()
            v.frame.origin.x = x
            x += AloeDeviceUtil.windowWidth()
            sv.addSubview(v)
            pageList.append(page)
        }
        sv.contentSize = CGSizeMake(x, AloeDeviceUtil.windowHeight())

        pager.reload(siteList)
        self.reloadView(true)
    }
    
    func reloadView(initial:Bool){
        let p:Int = currentPage()
        var to:Int = p + 1
        var from:Int = p - 1
        if(p == 0){
            from = p
        }
        if(p == siteCount-1){
            to = p
        }
        
        let siteList:SiteList = Model.sharedInstance.getSiteList()
        for index in from...to{
            if(index == p && !initial){
                continue;
            }
            println("siteCount:\(siteCount) index:\(index)")
            let page:Page = pageList[index]
            let site:Site = siteList.get(index)
            page.reload(site)
        }
        
        pager.active(p)
    }
    
    private func currentPage()->Int{
        let x:CGFloat = sv.contentOffset.x
        return Int(x / sv.frame.size.width)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        scrollView.scrollEnabled = true
        
        let x:CGFloat = scrollView.contentOffset.x
        let p:Int = Int(x / scrollView.frame.size.width)
        
        let page:Page = pageList[p]
        let pageView:UIView = page.getView()
        if(pageView.superview == sv){
            return
        }
        
        var frame:CGRect = self.pageFrame()
        frame.origin.x = frame.size.width * CGFloat(p)
        pageView.frame = frame
        sv.addSubview(pageView)
        
        self.reloadView(false)
    }
    
    func toTop(){
        AloeUtil.pain(toTopButton)
        let p:Int = self.currentPage()
        let page:Page = pageList[p]
        page.toTop()
        AloeThreadUtil.wait(0.2, block: { () -> () in
            self.hideToTopButton()
        })
    }
    
    func toFavorit(){
        let site:Site = Site.createFavoritSite()
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        Model.sharedInstance.createFavoritList { (articleList) -> () in
            site.setArticleList(articleList)
            let page:Page = Page(delegate: self)
            page.reload(site)
            page.modal()
            page.favorit()
            self.rootViewController.presentViewController(page.getViewController(), animated: true) { () -> Void in
                
            }
        }
    }
    
    func toSearch(){
        searchView.show()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {

        let x:CGFloat = scrollView.contentOffset.x
        let svWidth:CGFloat = scrollView.frame.size.width
        var p:Int = Int(x) / Int(svWidth)
        let amari = Int(x) % Int(svWidth)
        
        let per:CGFloat = CGFloat(amari) / svWidth
        let scale:CGFloat = 1.0 - (per / 6.0)
        
        let pageFrame:CGRect = self.pageFrame()
        let page:Page = pageList[p]
        let pView:UIView = page.getView()
        
        if(pView.superview != backView){
            backView.addSubview(pView)
            pView.frame = pageFrame
        }
        
        pView.transform = CGAffineTransformMakeScale(scale, scale)
        page.dark(1.0-scale)

        p = Int((x + svWidth / 2.0) / svWidth)
        if(p<0){
            p = 0
        }
        if(p >= siteCount){
            p = siteCount - 1
        }
        
    }
    
    func scrollViewWillBeginDecelerating(scrollView: UIScrollView) {
        scrollView.scrollEnabled = false;
    }
    
    private func pageFrame()->CGRect{
        return CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), AloeDeviceUtil.windowHeight())
    }
    
    func memoryWarning(){
        println("memoryWarning2")
        AloeImageCache.clear()
    }
    
    func willEnterForeground(){
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        self.updateModel()
        AloeUtil.activeApp()
    }
    
    func didEnterBackground(){
        
    }
    
    func onScrollUp(){
        self.showToTopButton()
    }
    
    func onScrollDown(){
        self.hideToTopButton()
    }
    
    func onShowDetail(){
        sv.scrollEnabled = false
        pager.hide()
        self.hideToTopButton()
        
        nativeAdView.hide()
    }
    
    func onHideDetail(){
        sv.scrollEnabled = true
        pager.show()
        self.showToTopButton()
        self.tryShowReview()
        
        nativeAdView.tryShow()
    }
    
    func showToTopButton(){
        if(floatingBox.alpha != 0){
            return
        }
        AloeTween.doTween(0.2, ease: AloeEase.OutCirc, { (val) -> () in
            self.floatingBox.transform = CGAffineTransformMakeTranslation(0, 20 - (20*val))
            self.floatingBox.alpha = val
        })
    }
    
    func hideToTopButton(){
        if(floatingBox.alpha != 1){
            return
        }
        AloeTween.doTween(0.2, ease: AloeEase.InCirc, { (val) -> () in
            self.floatingBox.transform = CGAffineTransformMakeTranslation(0, -20*val)
            self.floatingBox.alpha = 1.0 - val
        })
        
    }
    
    func foundArticleList(keyword:String, articleList: ArticleList) {
        println("found")
        let site:Site = Site.createSearchSite(keyword)
        site.setArticleList(articleList)
        let page:Page = Page(delegate: self)
        page.reload(site)
        page.modal()
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        rootViewController.presentViewController(page.getViewController(), animated: true) { () -> Void in
            
        }
    }
}