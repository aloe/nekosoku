//
//  Site.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation

class Site: AloeModel {
    
    let id:Int
    let name:String
    let excerpt:String
    let iconUrl:String
    let priority:Int
    var articleList:ArticleList?
    var nativeAdList:NativeAdList?
    var isAd:Bool = false
    
    init(nsdic:NSDictionary){
        id = nsdic["id"] as Int
        name = nsdic["name"] as String
        excerpt = nsdic["excerpt"] as String
        iconUrl = nsdic["icon_url"] as String
        priority = nsdic["priority"] as Int
        
        super.init()
    }
    
    func setArticleList(articleList:ArticleList){
        self.articleList = articleList
    }
    
    func lastupdatedStr()->String{
        if(isAd){
            return "無料っていいよね"
        }
        if articleList!.count() == 0{
            return ""
        }
        let article:Article = articleList!.get(0)
        let str:String = AloeDateUtil.dateToString(article.publishedAt, format: "M.dd")
        return str
    }
    
    class func createRootSite()->Site{
        let site:Site = Site(nsdic: ["id":0, "name":"Top", "excerpt":"ネコ好きによるネコ好きのためのネコまとめ", "icon_url":"", "priority":1000])
        return site
    }
    
    class func createAdSite()->Site{
        let site:Site = Site(nsdic: ["id":0, "name":"おすすめ無料アプリ", "excerpt":"暇つぶしのお供に", "icon_url":"", "priority":-1])
        site.isAd = true
        return site
    }
    
    class func createSearchSite(keyword:String)->Site{
        let site:Site = Site(nsdic: ["id":0, "name":"検索結果", "excerpt":"【\(keyword)】で検索", "icon_url":"", "priority":1000])
        return site
    }
    
    class func createFavoritSite()->Site{
        let site:Site = Site(nsdic: ["id":0, "name":"お気に入り", "excerpt":"記事画面右下のシェアボタンから登録できるよ", "icon_url":"", "priority":1000])
        return site
    }
    
}
