//
//  Article.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation

class Article: AloeModel {
    
    let id:Int
    let siteId:Int
    let title:String
    let excerpt:String
    let linkUrl:String
    let imageUrl:String
    let publishedAt:NSDate
    var favorit:Bool = false
    
    init(nsdic:NSDictionary){        
        id = nsdic["id"] as Int
        siteId = nsdic["site_id"] as Int
        title = nsdic["title"] as String
        linkUrl = nsdic["link_url"] as String
        imageUrl = nsdic["image_url"] as String
        excerpt = nsdic["excerpt"] as String
        let published_at:Int = nsdic["published_at"] as Int
        publishedAt = NSDate(timeIntervalSince1970: Double(published_at))
        
        super.init()
    }
    
    func hasKeyword(keyword:String)->Bool{
        var nsKeyword:NSString = keyword as NSString
        var nsTitle:NSString = title as NSString
        return (nsTitle.rangeOfString(nsKeyword).location != NSNotFound)
    }
    
    func hasImage()->Bool{
        return (countElements(imageUrl) != 0)
    }
    
}