//
//  NativeAdList.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/02.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation

class NativeAdList{
    
    private var list:[NativeAd] = []
    
    init(){
        
    }
    
    init(array:[ChkRecordData]){
        for chkData in array{
            let nativeAd:NativeAd = NativeAd(chkData: chkData)
            self.add(nativeAd)
        }
    }
    
    func add(nativeAd:NativeAd){
        list.append(nativeAd)
    }
    
    func get(index:Int)->NativeAd{
        return list[index]
    }
    
    func count()->Int{
        return list.count
    }
    
    class func createDummy()->NativeAdList{
        let adList:NativeAdList = NativeAdList()
        adList.add(NativeAd(title:"Nまとめ", iconUrl: "http://a5.mzstatic.com/us/r30/Purple2/v4/ee/64/6a/ee646ad4-0504-07ae-8e5a-2ffb6fcf0ba2/mzl.vnkbdoug.175x175-75.jpg", linkUrl: "https://itunes.apple.com/jp/app/id634891205?l=ja&ls=1&mt=8", content: "「NEVERまとめ」より最高に笑えるスレを収録!!あなたはもう笑いましたか？ﾟ+｡(ﾉ´∀｀)ﾉ"))
        
        adList.add(NativeAd(title: "お買い物メモ", iconUrl: "http://a3.mzstatic.com/us/r30/Purple5/v4/ed/0c/82/ed0c820b-f5b5-3dc7-c06f-3c4cc9ec494e/mzl.dyaghjlz.350x350-75.jpg", linkUrl: "https://itunes.apple.com/jp/app/o-maii-wumemo-shou-shuki-fengno/id607634035?mt=8", content: "買い忘れがもう無くなる！？メモをとらないと買うものを忘れてしまう。でもメモをとるのが面倒くさい。"))
        
        return adList
    }
    
}