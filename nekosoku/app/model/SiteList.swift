//
//  SiteList.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation

class SiteList: AloeModelList {
    
    private var list:[Site]
    
    override init(){
        list = []
        super.init()
    }
    
    init(nsarray:NSArray){
        list = []
        super.init()
        for row in nsarray{
            let site:Site = Site(nsdic: row as NSDictionary)
            self.add(site)
        }
    }
    
    func add(site:Site){
        list.append(site)
    }
    
    func addAt(site:Site, index:Int){
        list.insert(site, atIndex: index)
    }
    
    func get(index:Int)->Site{
        return list[index]
    }
    
    func count()->Int{
        return list.count
    }
    
    
}