//
//  ArticleList.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation

class ArticleList: AloeModelList {
    
    private var list:[Article]
    
    override init(){
        list = []
        super.init()
    }
    
    init(nsarray:NSArray){
        list = []
        super.init()
        for row in nsarray{
            let article:Article = Article(nsdic: row as NSDictionary)
            self.add(article)
        }
    }
    
    func add(article:Article){
        list.append(article)
    }
    
    func get(index:Int)->Article{
        return list[index]
    }
    
    func count()->Int{
        return list.count
    }
    
    func remove(index:Int){
        list.removeAtIndex(index)
    }
    
}