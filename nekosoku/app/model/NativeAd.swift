//
//  NativeAd.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/02.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation

class NativeAd{

    let title:String
    let iconUrl:String
    var bannerUrl:String?
    let linkUrl:String
    let content:String
    
    init(chkData:ChkRecordData){
        title = chkData.title
        iconUrl = chkData.imageIcon
        linkUrl = chkData.linkUrl
        content = chkData.detail
        
        if(!chkData.hasNativeBanner){
            return
        }
        bannerUrl = chkData.nativeBannerUrl
    }
    
    init(title:String, iconUrl:String, linkUrl:String, content:String){
        self.title = title
        self.iconUrl = iconUrl
        self.linkUrl = linkUrl
        self.content = content
    }
    
    func hasBanner()->Bool{
        return (bannerUrl != nil)
    }
    
}