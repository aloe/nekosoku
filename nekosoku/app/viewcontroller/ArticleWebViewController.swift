//
//  ArticleWebViewController.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/01.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit
import Social

class ArticleWebViewController: BaseViewController, UIWebViewDelegate, UIScrollViewDelegate {

    let webView:UIWebView = UIWebView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), AloeDeviceUtil.windowHeight()-50))
    let footerView:UIView = UIView(frame: CGRectMake(0, AloeDeviceUtil.windowHeight()-50, AloeDeviceUtil.windowHeight(), 50))
    let webBackButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    let webNextButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    var article:Article?
    var initialLoad:Bool = false
    
    func setup(){
        self.setupWebView()
        self.setupFooter()
        self.view.backgroundColor = UIColor.whiteColor()
        let reco:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "swipeRight")
        reco.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(reco)
    }
    
    func swipeRight(){
        self.navigationController?.popViewControllerAnimated(true)
        AloeEventUtil.dispatchGlobalEvent(ON_HIDE_DETAIL_EVENT)
    }
    
    private func setupWebView(){
        webView.backgroundColor = UIColor.whiteColor()
        webView.delegate = self
        webView.scrollView.delegate = self
        self.view.addSubview(webView)
    }
    
    private func setupFooter(){
        footerView.backgroundColor = UIColorFromHex(0xf7f7f6)
        self.view.addSubview(footerView)
        
        let margin:CGFloat = (320.0 - AloeDeviceUtil.windowWidth()) / 4
        var buttonFrame:CGRect = CGRectMake(0, 0, 64, 50)
        
        let backButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        backButton.frame = buttonFrame
        backButton.setImage(UIImage(named: "backButton"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: "tapBack", forControlEvents: UIControlEvents.TouchUpInside)
        footerView.addSubview(backButton)
        buttonFrame.origin.x += 64.0 + margin
        
        webBackButton.frame = buttonFrame
        webBackButton.setImage(UIImage(named: "webBackButton"), forState: UIControlState.Normal)
        webBackButton.addTarget(self, action: "tapWebBack", forControlEvents: UIControlEvents.TouchUpInside)
        webBackButton.enabled = false
        footerView.addSubview(webBackButton)
        buttonFrame.origin.x += 64.0 + margin
        
        webNextButton.frame = buttonFrame
        webNextButton.setImage(UIImage(named: "webNextButton"), forState: UIControlState.Normal)
        webNextButton.addTarget(self, action: "tapWebNext", forControlEvents: UIControlEvents.TouchUpInside)
        webNextButton.enabled = false
        footerView.addSubview(webNextButton)
        buttonFrame.origin.x += 64.0 + margin
        
        let reloadButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        reloadButton.frame = buttonFrame
        reloadButton.setImage(UIImage(named: "webReloadButton"), forState: UIControlState.Normal)
        reloadButton.addTarget(self, action: "tapWebReload", forControlEvents: UIControlEvents.TouchUpInside)
        footerView.addSubview(reloadButton)
        buttonFrame.origin.x += 64.0 + margin
        
        let shareButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        shareButton.frame = buttonFrame
        shareButton.setImage(UIImage(named: "webShareButton"), forState: UIControlState.Normal)
        shareButton.addTarget(self, action: "tapWebShare", forControlEvents: UIControlEvents.TouchUpInside)
        footerView.addSubview(shareButton)
        buttonFrame.origin.x += 64.0 + margin
    }
    
    func tapBack(){
        self.navigationController?.popViewControllerAnimated(true)
        AloeEventUtil.dispatchGlobalEvent(ON_HIDE_DETAIL_EVENT)
    }
    
    func tapWebBack(){
        if(webView.canGoBack){
            self.showLoading()
            webView.goBack()
        }
    }
    
    func tapWebNext(){
        if(webView.canGoForward){
            self.showLoading()
            webView.goForward()
        }
    }
    
    func tapWebReload(){
        webView.loadRequest(NSURLRequest(URL: NSURL(string: article!.linkUrl)))
        self.showLoading()
    }
    
    func tapWebShare(){
        let title:String = webView.stringByEvaluatingJavaScriptFromString("document.title")!
        let url:String = webView.stringByEvaluatingJavaScriptFromString("document.URL")!
        
        let sheet:UIAlertController = UIAlertController(title: "シェアする", message: title, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        // お気に入り
        var t:String = (article!.favorit) ? "お気に入りから削除" : "お気に入りに追加"
        sheet.addAction(UIAlertAction(title: t, style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            Model.sharedInstance.toggleFavorit(self.article!)
        }))
        
        // Twitter
        sheet.addAction(UIAlertAction(title: "Twitter", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let vc:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            vc.setInitialText(title)
            vc.addURL(NSURL(string: url))
            AloeImageCache.loadImage(self.article!.imageUrl, callback: { (image, url, useCache) -> () in
                vc.addImage(image)
                self.presentViewController(vc, animated: true, completion: { () -> Void in
                    
                })
            }, failCallback: { (error) -> () in
                self.presentViewController(vc, animated: true, completion: { () -> Void in
                    
                })
            })
        }))
        
        // Facebook
        sheet.addAction(UIAlertAction(title: "Facebook", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let vc:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            vc.setInitialText(title)
            vc.addURL(NSURL(string: url))
            AloeImageCache.loadImage(self.article!.imageUrl, callback: { (image, url, useCache) -> () in
                vc.addImage(image)
                self.presentViewController(vc, animated: true, completion: { () -> Void in
                    
                })
                }, failCallback: { (error) -> () in
                    self.presentViewController(vc, animated: true, completion: { () -> Void in
                        
                    })
            })
        }))
        
        sheet.addAction(UIAlertAction(title: "違反報告", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let c:AloeURLLoadCommand = AloeURLLoadCommand(url_: Api.url("ihan.rb?title=" + self.article!.title))
            c.execute()
            let alert:UIAlertController = UIAlertController(title: "報告しました", message: "ご協力ありがとうございます", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (actioin) -> Void in
                
            }))
            self.presentViewController(alert, animated: true, completion: { () -> Void in
                
            })
            
        }))
        sheet.addAction(UIAlertAction(title: "キャンセル", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
            
        }))
        
        self.presentViewController(sheet, animated: true) { () -> Void in
            
        }
    
    }
    
    func reload(article:Article){
        self.article = article
        webView.loadRequest(NSURLRequest(URL: NSURL(string: article.linkUrl)))
        self.showLoading()
        initialLoad = true
        webView.transform = CGAffineTransformMakeScale(0.9, 0.9)
        webView.alpha = 0
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool{
        if(navigationType == UIWebViewNavigationType.LinkClicked){
            self.showLoading()
        }

        return true
    }
    
    func webViewDidStartLoad(webView: UIWebView){
        
    }
    
    func webViewDidFinishLoad(webView: UIWebView){
        self.hideLoading()
        self.noHorizonScroll()
        webBackButton.enabled = webView.canGoBack
        webNextButton.enabled = webView.canGoForward
        
        self.noAd()
        if(!initialLoad){
            return
        }
        webView.transform = CGAffineTransformMakeScale(0.9, 0.9)
        webView.alpha = 0
        AloeTweenChain.create().wait(0.2).add(0.2, ease: AloeEase.OutCubic, { (val) -> () in
            let scale:CGFloat = 0.9 + (0.1*val)
            self.webView.transform = CGAffineTransformMakeScale(scale, scale)
            self.webView.alpha = val
        }).execute()
        
        initialLoad = false
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError){
        self.hideLoading()
    }
    
    private func noHorizonScroll(){
        var size:CGSize = webView.scrollView.contentSize
        size.width = 320
        webView.scrollView.contentSize = size
    }
    
    private func noAd(){
        
        let select = "div[id*=\"adroute_ads_adblock\"]"
        var scriptList:[String] = []
        scriptList.append("document.querySelector('.bottom_fixed_ad').parentNode.removeChild(document.querySelector('.bottom_fixed_ad'));")
        scriptList.append("document.querySelector('.fluct_ad_container').parentNode.removeChild(document.querySelector('.fluct_ad_container'));")
        scriptList.append("document.querySelector('.adfunnel_sp').parentNode.removeChild(document.querySelector('.adfunnel_sp'));")
        scriptList.append("document.querySelector('#ad').parentNode.removeChild(document.querySelector('#ad'));")
        scriptList.append("document.querySelector('\(select)').parentNode.removeChild(document.querySelector('\(select)'));")
        scriptList.append("document.querySelector('\(select)').parentNode.removeChild(document.querySelector('\(select)'));")
        
        for s in scriptList{
            webView.stringByEvaluatingJavaScriptFromString(s)
        }
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = true        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.noAd()
    }
    
    deinit{
        println("webView deinit")
    }

}
