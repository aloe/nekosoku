//
//  ArticleListViewController.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

protocol ArticleListDelegate{
    
    func onSelectArticle(article:Article)
    
}

private let cellIdentifier:String = "NewsListCell"
private let cellIdentifierAd:String = "AdListCell"
private let adstirBannerCell:AdstirBannerCell = AdstirBannerCell()

class ArticleListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource{
    
    private var delegate:ArticleListDelegate?
    private var site:Site?
    private var tableView:UITableView = UITableView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), AloeDeviceUtil.windowHeight()))
    private let headerView:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), 110))
    private let titleLabel:UILabel = UILabel(frame:CGRectMake(5, 20, AloeDeviceUtil.windowWidth()-10, 40))
    private let excerptLabel:UILabel = UILabel(frame: CGRectMake(5, 20+40, AloeDeviceUtil.windowWidth()-10, 20))
    private let lastupdatedLabel:UILabel = UILabel(frame: CGRectMake(5, 20+40+20, AloeDeviceUtil.windowWidth()-10, 30))
    private let adcropsBannerCell:AdcropsBannerCell = AdcropsBannerCell()
    private var isFavorit:Bool = false
    
    func setup(delegate:ArticleListDelegate){
        self.delegate = delegate
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 32, 0)
        self.view.addSubview(tableView)
        
        headerView.backgroundColor = UIColor.whiteColor()
        tableView.tableHeaderView = headerView
        headerView.addSubview(titleLabel)
        headerView.addSubview(excerptLabel)
        headerView.addSubview(lastupdatedLabel)
        
        titleLabel.font = UIFont.boldSystemFontOfSize(20.0)
        excerptLabel.font = UIFont.systemFontOfSize(14.0)
        lastupdatedLabel.font = UIFont.boldSystemFontOfSize(18.0)
        titleLabel.textColor = UIColorFromHex(0x252525)
        excerptLabel.textColor = UIColorFromHex(0x252525)
        lastupdatedLabel.textColor = UIColorFromHex(0x252525)
        
        titleLabel.textAlignment = NSTextAlignment.Center
        excerptLabel.textAlignment = NSTextAlignment.Center
        lastupdatedLabel.textAlignment = NSTextAlignment.Center
        
        adcropsBannerCell.setup()
        if(!adstirBannerCell.isSetuped()){
            adstirBannerCell.setup()
        }
    }
    
    func reload(site:Site){
        self.site = site
        tableView.reloadData()
        
        titleLabel.text = site.name
        excerptLabel.text = site.excerpt
        lastupdatedLabel.text = site.lastupdatedStr()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        if(site!.isAd){
            return AdCell.height()
        }
        
        if(self.isAdIndex(indexPath)){
            if(self.isAdstirIndex(indexPath)){
                return adstirBannerCell.height()
            }else{
                return adcropsBannerCell.height()
            }
        }
        return NekoUtil.cellHeight()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(site == nil){
            return 0
        }
        
        if(site!.isAd){
            if(Ad.sharedInstance.isReady()){
                return Ad.sharedInstance.getIconAdList().count()
            }else{
                return 0
            }
        }
        
        return site!.articleList!.count()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        if(site!.isAd){
            var cell:AdCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifierAd) as AdCell?
            if cell == nil {
                cell = AdCell(style:UITableViewCellStyle.Default, reuseIdentifier:cellIdentifier)
                cell!.setup()
            }
            let nativeAd:NativeAd = Ad.sharedInstance.getIconAdList().get(indexPath.row)
            cell!.reload(nativeAd, index:indexPath.row)
            return cell!
        }
        
        if(self.isAdIndex(indexPath)){
            if(self.isAdstirIndex(indexPath)){
                adstirBannerCell.show()
                return adstirBannerCell
            }else{
                adcropsBannerCell.reload(Ad.sharedInstance.getNativeAd())
                return adcropsBannerCell
            }
        }
        
        var cell:ArticleCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as ArticleCell?
        if cell == nil {
            cell = ArticleCell(style:UITableViewCellStyle.Default, reuseIdentifier:cellIdentifier)
            cell!.setup()
        }
        
        let article:Article = self.model(indexPath)
        cell!.reload(article)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(site!.isAd){
            let cell:AdCell = tableView.cellForRowAtIndexPath(indexPath) as AdCell
            cell.hilight()
            let nativeAd:NativeAd = Ad.sharedInstance.getIconAdList().get(indexPath.row)
            AloeUtil.openBrowser(nativeAd.linkUrl)
            return
        }
        
        if(self.isAdstirIndex(indexPath)){
            adstirBannerCell.hilight()
            return
        }
        if(self.isAdIndex(indexPath)){
            adcropsBannerCell.hilight()
            let nativeAd:NativeAd = adcropsBannerCell.nativeAd!
            AloeUtil.openBrowser(nativeAd.linkUrl)
            return
        }
        var cell:ArticleCell = tableView.cellForRowAtIndexPath(indexPath) as ArticleCell
        let article:Article = self.model(indexPath)
        cell.hilight()
        
        delegate!.onSelectArticle(article)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return isFavorit && !self.isAdIndex(indexPath)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete{
            let article:Article = self.model(indexPath)
            Model.sharedInstance.toggleFavorit(article)
            let index:Int = self.modelIndex(indexPath)
            site!.articleList!.remove(index)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)
        }
    }
    
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y > 0){
            AloeEventUtil.dispatchGlobalEvent(ON_SCROLL_DOWN_EVENT)
        }else{
            AloeEventUtil.dispatchGlobalEvent(ON_SCROLL_UP_EVENT)
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if(scrollView.contentOffset.y == 0){
            AloeEventUtil.dispatchGlobalEvent(ON_SCROLL_DOWN_EVENT)
        }
    }
    
    private func isAdIndex(indexPath:NSIndexPath)->Bool{
        let row:Int = indexPath.row
        return (row != 0 && row % AD_PER_LIST == 0)
    }
    
    private func isAdstirIndex(indexPath:NSIndexPath)->Bool{
        if(!self.isAdIndex(indexPath)){
            return false
        }
        if(!Ad.sharedInstance.isReady()){
            return true
        }
        
        return true // とりあえずここはadstireのみにする
//        return (indexPath.row % (AD_PER_LIST * ADSTIR_PER_AD) == 0)
    }
    
    private func modelIndex(indexPath:NSIndexPath)->Int{
        let row:Int = indexPath.row
        let modelIndex:Int = row - (row / AD_PER_LIST)
        return modelIndex
    }
    
    private func model(indexPath:NSIndexPath)->Article{
        return site!.articleList!.get(self.modelIndex(indexPath))
    }
    
    func toTop(){
        tableView.setContentOffset(CGPointMake(0, 0), animated: true)
    }

    func modal(){
        headerView.backgroundColor = UIColorFromHex(0x252525)
        titleLabel.textColor = UIColorFromHex(0xeeeeee)
        excerptLabel.textColor = UIColorFromHex(0xeeeeee)
        lastupdatedLabel.textColor = UIColorFromHex(0xeeeeee)
        lastupdatedLabel.text = String(site!.articleList!.count()) + "件"
        tableView.backgroundColor = UIColorFromHex(0x252525)
    }
    
    func favorit(){
        isFavorit = true
    }

}
