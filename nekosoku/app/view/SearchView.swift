//
//  SearchView.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/07.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

protocol SearchViewDelegate{
    
    func foundArticleList(keyword:String, articleList:ArticleList)
    
}

class SearchView: NSObject, UITextFieldDelegate {
    
    var delegate:SearchViewDelegate?
    let bg:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), AloeDeviceUtil.windowHeight()))
    let view:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), AloeDeviceUtil.windowHeight()))
    let tf:UITextField = UITextField()
    
    override init(){
        super.init()
    }
    
    func setup(){
        bg.backgroundColor = UIColor.blackColor()
        bg.alpha = 0
        view.addSubview(bg)
        bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "hide"))
        
        tf.delegate = self
        tf.frame = CGRectMake(20, (AloeDeviceUtil.windowHeight()-(55*3)) / 2, AloeDeviceUtil.windowWidth()-40, 45)
        tf.borderStyle = UITextBorderStyle.RoundedRect
        tf.alpha = 0
        tf.returnKeyType = UIReturnKeyType.Search
        tf.clearButtonMode = UITextFieldViewMode.Always
        view.addSubview(tf)
        
        view.userInteractionEnabled = false
    }
    
    func getView()->UIView{
        return view
    }
    
    func show(){
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        view.userInteractionEnabled = true
        self.tf.transform = CGAffineTransformMakeTranslation(0, 20)
        self.tf.becomeFirstResponder()
        AloeTween.doTween(0.2, ease: AloeEase.OutCirc) { (val) -> () in
            self.bg.alpha = val*0.7
            self.tf.transform = CGAffineTransformMakeTranslation(0, 20-(20*val))
            self.tf.alpha = val
        }
    }
    
    func hide(){
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.Default, animated: true)
        view.userInteractionEnabled = false
        tf.resignFirstResponder()
        AloeTween.doTween(0.2, ease: AloeEase.OutCirc) { (val) -> () in
            self.bg.alpha = 0.7 - (val*0.7)
            self.tf.alpha = 1.0 - val
            self.tf.transform = CGAffineTransformMakeTranslation(0, 0)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.search()
        
        return true
    }
    
    func search(){
        let keyword = tf.text
        Model.sharedInstance.search(keyword, callback: { (articleList) -> () in
            
            if articleList.count() != 0 {
                self.hide()
                self.delegate!.foundArticleList(keyword, articleList: articleList)
                return
            }
            
            let alert:UIAlertController = UIAlertController(title: "", message: "お探しの情報は見つかりませんでした", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
            }))
            
            NekoUtil.getRootViewController().presentViewController(alert, animated: true, completion: { () -> Void in
                
            })
        })
    }
    
}
