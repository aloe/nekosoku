//
//  NativeAdView.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/08.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation


private let tryNativeAdNumKey:String = "tryNativeAdNumKey"
class NativeAdView:NSObject{
    
    let view:UIView = UIView(frame: CGRectMake(0, AloeDeviceUtil.windowHeight()-85-20, AloeDeviceUtil.windowHeight(), 105))
    let iconView:UIImageView = UIImageView(frame: CGRectMake(10, 10, 40, 40))
    let titleLabel:UILabel = UILabel(frame: CGRectMake(60, 10, AloeDeviceUtil.windowWidth()-60-10-15, 15))
    let timeLabel:UILabel = UILabel(frame:CGRectMake(AloeDeviceUtil.windowWidth()-10-8, 8, 15, 15))
    let contentLabel:UILabel = UILabel(frame: CGRectMake(60, 28, AloeDeviceUtil.windowWidth()-60-10, 45))
    var nativeAd:NativeAd?
    var timer:NSTimer?
    var current:Int = 10
    
    override init(){
        let bgView:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), 125))
        bgView.backgroundColor = UIColor.blackColor()
        bgView.alpha = 0.7
        view.addSubview(bgView)
        
        titleLabel.font = UIFont.boldSystemFontOfSize(13.0)
        titleLabel.textColor = UIColor.whiteColor()
        contentLabel.font = UIFont.systemFontOfSize(12.0)
        contentLabel.textColor = UIColor.whiteColor()
        contentLabel.numberOfLines = 0
        
        timeLabel.font = UIFont.boldSystemFontOfSize(12.0)
        timeLabel.textColor = UIColor.whiteColor()
        timeLabel.textAlignment = NSTextAlignment.Right
        
        view.transform = CGAffineTransformMakeTranslation(0, 105)
        
        view.addSubview(iconView)
        view.addSubview(titleLabel)
        view.addSubview(timeLabel)
        view.addSubview(contentLabel)
        
        super.init()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tap"))
    }
    
    func tap(){
        AloeUtil.openBrowser(nativeAd!.linkUrl)
    }
    
    func startTimer(){
        self.stopTimer()
        timeLabel.text = String(current)
        timer = NSTimer(timeInterval: 1.0, target: self, selector: "updateTimelabel", userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timer!, forMode: NSRunLoopCommonModes)
    }
    
    @objc func updateTimelabel(){
        current--;
        timeLabel.text = String(current)
        if(current == 0){
            self.hide()
        }
    }
    
    func stopTimer(){
        current = 10
        if(timer != nil){
            timer!.invalidate()
            timer = nil
        }
    }
    
    func tryShow(){
        if(!AloeUtil.isJaDevice()){
            return
        }
        var ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var num:Int = ud.integerForKey(tryNativeAdNumKey)
        num++
        ud.setInteger(num, forKey: tryNativeAdNumKey)
        ud.synchronize()
        self.show()
//        if arc4random_uniform(10) < 5 {
//            self.show()
//        }
    }
    
    private func show(){
        if(!Ad.sharedInstance.isReady()){
            return
        }
        self.startTimer()
        self.reload()
        AloeTween.doTween(0.3, ease: AloeEase.OutBack) { (val) -> () in
            self.view.transform = CGAffineTransformMakeTranslation(0, 105 - (105 * val))
        }
    }
    
    func hide(){
        self.stopTimer()
        if(view.transform.ty != 0){
            return
        }
        AloeTween.doTween(0.2, ease: AloeEase.InBack) { (val) -> () in
            let translation:CGAffineTransform = CGAffineTransformMakeTranslation(0, 105*val)
            self.view.transform = translation
        }
    }
    
    func reload(){
        nativeAd = Ad.sharedInstance.getNativeAd()
        titleLabel.text = "【無料】" + nativeAd!.title
        contentLabel.text = nativeAd!.content
        
        iconView.image = nil
        AloeImageCache.loadImage(nativeAd!.iconUrl, callback: { (image, url, useCache) -> () in
            if(self.nativeAd!.iconUrl != url){
                return
            }
            self.iconView.image = image
        }) { (error) -> () in
            
        }
    }
    
    func getView()->UIView{
        return view
    }
}