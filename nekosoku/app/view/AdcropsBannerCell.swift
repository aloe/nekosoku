//
//  AdcropsBannerCell.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/02.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

private let AdcropsBannerCellHeightRate:CGFloat = 0.51
private let cellHeight:CGFloat = AloeDeviceUtil.windowWidth()*AdcropsBannerCellHeightRate

class AdcropsBannerCell: UITableViewCell {
    
    private let bannerImageView:UIImageView = UIImageView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), cellHeight))
    var nativeAd:NativeAd?
    private let dateLabel:UILabel = UILabel(frame: CGRectMake(AloeDeviceUtil.windowWidth()-50, 15, 50, 30))
    private let dateLabelBg:UIView = UIView()
    private let hilightView:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), cellHeight))
    private let indicator:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    
    func setup(){
        let view:UIView = self.contentView
        
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        indicator.frame = bannerImageView.frame
        view.addSubview(indicator)
        
        view.addSubview(bannerImageView)
        
        dateLabel.font = UIFont.boldSystemFontOfSize(16.0)
        dateLabel.textColor = UIColorFromHex(0x252525)
        dateLabel.textAlignment = NSTextAlignment.Center
        dateLabel.text = "PR"
        dateLabelBg.frame = CGRectMake(AloeDeviceUtil.windowWidth()-50, 15, 50 + 15, 30)
        dateLabelBg.backgroundColor = UIColor.whiteColor()
        dateLabelBg.alpha = 0.8
        dateLabelBg.layer.cornerRadius = 15
        view.addSubview(dateLabelBg)
        view.addSubview(dateLabel)
        
        hilightView.backgroundColor = UIColor.whiteColor()
        hilightView.alpha = 0
        hilightView.userInteractionEnabled = false
        view.addSubview(hilightView)
    }
    
    func height()->CGFloat{
        return cellHeight
    }
    
    func reload(nativeAd:NativeAd){
        self.nativeAd = nativeAd
        if(!nativeAd.hasBanner()){
            return
        }
        
        indicator.startAnimating()
        bannerImageView.image = nil
        bannerImageView.alpha = 0
        bannerImageView.transform = CGAffineTransformMakeScale(0.9, 0.9)
        AloeImageCache.loadImage(self.nativeAd!.bannerUrl!, callback: { (image, url, useCache) -> () in
            if(url != self.nativeAd!.bannerUrl!){
                return
            }
            self.bannerImageView.image = image
            self.indicator.stopAnimating()
            if(useCache){
                self.bannerImageView.alpha = 1.0
                self.bannerImageView.transform = CGAffineTransformMakeScale(1.0, 1.0)
                return
            }
            AloeTween.doTween(0.2, ease: AloeEase.OutQuint, progress: { (val) -> () in
                self.bannerImageView.alpha = val
                self.bannerImageView.transform = CGAffineTransformMakeScale(0.9+(val*0.1), 0.9+(val*0.1))
            })
        }) { (error) -> () in
            self.indicator.stopAnimating()
        }
    }
    
    func hilight(){
        AloeTween.doTween(0.3, ease: AloeEase.InQuart) { (val) -> () in
            let fromAlpha:CGFloat = 0.8
            self.hilightView.alpha = fromAlpha - (val * fromAlpha)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
