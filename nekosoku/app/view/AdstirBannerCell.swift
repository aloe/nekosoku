//
//  AdstirBannerCell.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/02.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

private let scale:CGFloat = AloeDeviceUtil.windowWidth() / 300
private let AdstirBannerCellHeightRate:CGFloat = 0.78
private let AdstirBannerCellHeight:CGFloat = AloeDeviceUtil.windowWidth() * AdstirBannerCellHeightRate * scale

class AdstirBannerCell: UITableViewCell, AdstirMraidViewDelegate {
    
    private let adView:AdstirMraidView = AdstirMraidView(adSize: kAdstirAdSize300x250, media: ADSTIR_MEDIA_ID, spot: ADSTIR_SPOT_NO)
    private var isSetuped_:Bool = false
    private let dateLabel:UILabel = UILabel(frame: CGRectMake(AloeDeviceUtil.windowWidth()-50, 15, 50, 30))
    private let dateLabelBg:UIView = UIView()
    private let hilightView:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), AdstirBannerCellHeight))
    private let indicator:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    
    func setup(){
        
        self.selectionStyle = UITableViewCellSelectionStyle.None
        let view:UIView = self.contentView
        
        indicator.frame = view.frame
        indicator.startAnimating()
        
        adView.frame.origin.x += (AloeDeviceUtil.windowWidth()-300) / 2
        adView.frame.origin.y += (self.height() - 250) / 2
        adView.transform = CGAffineTransformMakeScale(scale, scale)
        adView.delegate = self
        adView.start()
        
        view.addSubview(adView)
        
        dateLabel.font = UIFont.boldSystemFontOfSize(16.0)
        dateLabel.textColor = UIColorFromHex(0x252525)
        dateLabel.textAlignment = NSTextAlignment.Center
        dateLabel.text = "PR"
        dateLabelBg.frame = CGRectMake(AloeDeviceUtil.windowWidth()-50, 15, 50 + 15, 30)
        dateLabelBg.backgroundColor = UIColor.whiteColor()
        dateLabelBg.alpha = 0.8
        dateLabelBg.layer.cornerRadius = 15
        view.addSubview(dateLabelBg)
        view.addSubview(dateLabel)
        
        hilightView.backgroundColor = UIColor.whiteColor()
        hilightView.alpha = 0
        hilightView.userInteractionEnabled = false
        view.addSubview(hilightView)
        
        isSetuped_ = true
    }
    
    func show(){
        indicator.startAnimating()
    }

    func adstirMraidViewDidPresentScreen(mraidView: AdstirMraidView!) {
        println("adstirMraidViewDidPresentScreen")
    }
    
    func adstirMraidViewWillDismissScreen(mraidView: AdstirMraidView!) {
        println("adstirMraidViewWillDismissScreen")
    }
    
    func adstirMraidViewWillLeaveApplication(mraidView: AdstirMraidView!) {
        println("adstirMraidViewWillLeaveApplication")
    }
    
    func adstirMraidViewWillPresentScreen(mraidView: AdstirMraidView!) {
        println("adstirMraidViewWillPresentScreen")
    }
    
    func height()->CGFloat{
        return AdstirBannerCellHeight
    }
    
    func isSetuped()->Bool{
        return isSetuped_
    }
    
    func hilight(){
        AloeTween.doTween(0.3, ease: AloeEase.InQuart) { (val) -> () in
            let fromAlpha:CGFloat = 0.8
            self.hilightView.alpha = fromAlpha - (val * fromAlpha)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
