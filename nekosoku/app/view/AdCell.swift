//
//  AdCell.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/05.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class AdCell: UITableViewCell {
    
    var nativeAd:NativeAd?
    let iconView:UIImageView = UIImageView(frame: CGRectMake(10, 10, 60, 60))
    let indicator:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    let titleLabel:UILabel = UILabel(frame: CGRectMake(80, 10, AloeDeviceUtil.windowWidth()-80-10, 15))
    let contentLabel:UILabel = UILabel(frame: CGRectMake(80, 25, AloeDeviceUtil.windowWidth()-80-10, 60))
    let bgView:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), 95))
    let hilightView:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), 95))
    
    func setup(){
        self.selectionStyle = UITableViewCellSelectionStyle.None
        let view:UIView = self.contentView
        
        bgView.backgroundColor = UIColorFromHex(0xeeeeee)
        view.addSubview(bgView)
        
        titleLabel.textColor = UIColorFromHex(0x252525)
        titleLabel.font = UIFont.boldSystemFontOfSize(14.0)
        view.addSubview(titleLabel)
        
        contentLabel.numberOfLines = 4
        contentLabel.textColor = UIColorFromHex(0x252525)
        contentLabel.font = UIFont.systemFontOfSize(12.0)
        indicator.frame = iconView.frame
        view.addSubview(indicator)
        view.addSubview(iconView)
        view.addSubview(contentLabel)
        
        hilightView.backgroundColor = UIColor.whiteColor()
        hilightView.alpha = 0
        view.addSubview(hilightView)
    }
    
    func reload(nativeAd:NativeAd, index:Int){
        self.nativeAd = nativeAd
        
        bgView.alpha = (index % 2 == 0) ? 1.0 : 0.0
        
        titleLabel.text = nativeAd.title
        contentLabel.text = nativeAd.content
        iconView.alpha = 0
        iconView.transform = CGAffineTransformMakeScale(0.9, 0.9)
        iconView.image = nil
        AloeImageCache.loadImage(nativeAd.iconUrl, callback: { (image, url, useCache) -> () in
            if(url != nativeAd.iconUrl){
                return
            }
            self.iconView.image = image
            self.indicator.stopAnimating()
            if(useCache){
                self.iconView.alpha = 1.0
                self.iconView.transform = CGAffineTransformMakeScale(1.0, 1.0)
                return
            }
            AloeTween.doTween(0.2, ease: AloeEase.OutQuint, progress: { (val) -> () in
                self.iconView.alpha = val
                self.iconView.transform = CGAffineTransformMakeScale(0.9+(val*0.1), 0.9+(val*0.1))
            })
            }) {
                (error) -> () in
        }

    }
    
    class func height()->CGFloat{
        return 95
    }
    
    func hilight(){
        AloeTween.doTween(0.3, ease: AloeEase.InQuart) { (val) -> () in
            let fromAlpha:CGFloat = 0.8
            self.hilightView.alpha = fromAlpha - (val * fromAlpha)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
