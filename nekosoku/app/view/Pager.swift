//
//  Pager.swift
//  nekosoku
//
//  Created by kawase yu on 2014/09/29.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class Pager: NSObject {

    private class Potti:UIView{
        
        private var index:Int = 0
        
        func setup(index:Int){
            self.index = index
            self.frame = CGRectMake(CGFloat(index*11)+4, 2.5, 7, 7)
            self.layer.cornerRadius = 3.5
            self.backgroundColor = UIColorFromHex(0x444444)
        }
        
        func active(){
            println("active")
            self.alpha = 1.0
            self.backgroundColor = UIColorFromHex(0x990000)
        }
        
        func deactive(){
            self.alpha = 0.3
            self.backgroundColor = UIColorFromHex(0x444444)
        }
    }
    
    private var pottiList:[Potti] = []
    
    let view:UIView = UIView()
    let bgView:UIView = UIView()
    
    func setup(){
        view.addSubview(bgView)
        bgView.backgroundColor = UIColor.whiteColor()
        bgView.layer.cornerRadius = 6
        bgView.alpha = 0.8
    }

    func getView()->UIView{
        return view
    }
    
    func reload(siteList:SiteList){
        for v in view.subviews{
            v.removeFromSuperview()
        }
        view.addSubview(bgView)
        pottiList.removeAll()
        for index in 0..<siteList.count(){
            let potti:Potti = Potti()
            potti.setup(index)
            view.addSubview(potti)
            pottiList.append(potti)
        }
        
        view.frame = self.viewFrame()
        bgView.frame = CGRectMake(0, 0, self.viewWidth(), 12)
    }
    
    private func viewWidth()->CGFloat{
        return CGFloat(pottiList.count * 11) + 4.0
    }
    
    private func viewFrame()->CGRect{
        return CGRectMake((AloeDeviceUtil.windowWidth()-self.viewWidth()) / 2.0, AloeDeviceUtil.windowHeight()-12-10, self.viewWidth(), 12)
    }
    
    func active(index:Int){
        for i in 0..<pottiList.count{
            let potti:Potti = pottiList[i]
            potti.deactive()
            if(i == index){
                potti.active()
            }
        }
    }
    
    func show(){
//        view.frame = self.viewFrame()
//        view.transform = CGAffineTransformMakeTranslation(0, 20)
        AloeTween.doTween(0.2, ease: AloeEase.OutCubic) { (val) -> () in
//            self.view.transform = CGAffineTransformMakeTranslation(0, 20 - (20*val))
            self.view.alpha = val
        }
    }
    
    func hide(){
//        view.frame = self.viewFrame()
//        view.transform = CGAffineTransformMakeTranslation(0, 20)
        AloeTween.doTween(0.2, ease: AloeEase.OutCubic) { (val) -> () in
//            self.view.transform = CGAffineTransformMakeTranslation(0, -(20*val))
            self.view.alpha = 1.0 - val
        }
    }
    
}
